from __future__ import print_function

from collections import namedtuple

Point = namedtuple('Point', ['x', 'y'])

class Ship:
    AirCraftCarier = [' XXX', 'XXX ']
    Battleship = ['XXXXX']
    Cruiser = ['XXXX']
    Submarine = ['XXX', ' X ']
    Destroyer = ["XXX"]
    PatrolBoat = ["XX"]
    ALL_SHIPS_PATTERNS = [AirCraftCarier, Battleship, Cruiser, Submarine, Destroyer, PatrolBoat]

    def rotate(self):
        self.parse_pattern(list(zip(*self.shape[::-1])))

    def parse_pattern(self, pattern):
        self.h = len(pattern)
        self.w = len(pattern[0])
        shape = []
        # How many blood this ship has. Initially # of cells.
        self.blood = 0
        for line in pattern:
            assert len(line) == self.w
            l = [False] * self.w
            for i in range(len(line)):
                if line[i] != ' ' and line[i] != False:
                    l[i] = True
                    self.blood += 1
            shape.append(l)
        self.shape = shape

    def __init__(self, pattern):
        self.pos = (0, 0)
        self.parse_pattern(pattern)
        self.pattern = pattern

    def all_positions(self):
        for i in range(self.h):
            for j in range(self.w):
                if self.shape[i][j]:
                    yield Point(self.pos.x + i, self.pos.y + j)

    def shot(self):
        self.blood -= 1

    def is_sunk(self):
        return self.blood <= 0

    def __str__(self):
        return "Ship @ " + str(self.pos) + "\n" + "\n".join(
            (''.join((('X' if p else " ") for p in s)) for s in self.shape))


def test_ship_pattern():
    s = Ship(Ship.Submarine)
    print(str(s))
    for i in range(3):
        s.rotate()
        print(str(s))

def main():
    test_ship_pattern()


if __name__ == "__main__":
    main()
