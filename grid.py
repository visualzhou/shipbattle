from __future__ import print_function

from enum import Enum
import random

from ship import *

class ShotState(Enum):
    UNVISITED = 0
    HIT = 1
    MISS = 2
    SUNK = 3

class Cell:
    def __init__(self):
        self.shot_state = ShotState.UNVISITED
        self.occupied_by = None

    def occupy_by(self, ship):
        self.occupied_by = ship

    def shot(self):
        if self.shot_state != ShotState.UNVISITED:
            print("Cannot shot a cell more than once!")
            return None

        if self.occupied_by:
            self.shot_state = ShotState.HIT
            self.occupied_by.shot()
        else:
            self.shot_state = ShotState.MISS
        return self.shot_state

    def sym(self):
        return { ShotState.UNVISITED: " ",
                 ShotState.HIT: "O",
                 ShotState.MISS: "X",
                 ShotState.SUNK: "S" }[self.shot_state]

class Grid:
    WIDTH = 10
    HEIHGT = 10

    def __init__(self):
        self.ships = []
        self.grid = [None] * Grid.HEIHGT
        for i in range(Grid.HEIHGT):
            self.grid[i] = []
            for j in range(Grid.WIDTH):
                self.grid[i].append(Cell())

    def place_ship(self, ship, dry_run = True):
        for x, y in ship.all_positions():
            if x < 0 or x >= Grid.HEIHGT:
                return False
            if y < 0 or y >= Grid.WIDTH:
                return False
            if self.grid[x][y].occupied_by != None:
                return False
            # Place the ship.
            if not dry_run:
                self.grid[x][y].occupied_by = ship

        if not dry_run:
            self.ships.append(ship)
        return True

    def random_place_ships(self, ships):
        for ship in ships:
            # Rotate the ship
            for i in range(random.randrange(4)):
                ship.rotate()
            place_result = False
            while not place_result:
                ship.pos = Point(random.randrange(10), random.randrange(10))
                place_result = self.place_ship(ship, True) # dry-run
                if place_result:
                    place_result = self.place_ship(ship, False)

    def display(self, show_ships = False):
        # Header
        def header(grid):
            print(" ", end="")
            for i in range(len(grid[0])):
                print(" " + str(i), end="")
            print("")

        header(self.grid)

        for i, line in enumerate(self.grid):
            print(str(i) + " ", end="")
            for cell in line:
                if show_ships:
                    # Show all ships
                    background = "#" if cell.occupied_by else "~"
                    print(background + cell.sym(), end="")
                else:
                    # Hide ships
                    print((cell.sym() if cell.sym() != " " else "~") + " ", end="")
            print(i)

        header(self.grid)


    def shot(self, pos):
        # TODO error check
        assert(pos.x >= 0 and pos.x < Grid.HEIHGT)
        assert(pos.y >= 0 and pos.y < Grid.WIDTH)
        cell = self.grid[pos.x][pos.y]
        shot_result = cell.shot()
        ship = cell.occupied_by
        # Check sunk
        if shot_result == ShotState.HIT and ship.is_sunk():
            # Mark cells
            for x, y in ship.all_positions():
                self.grid[x][y].shot_state = ShotState.SUNK
        return shot_result

def test_grid_empty():
    grid = Grid()
    grid.display()

def test_grid_one_ship():
    grid = Grid()
    grid.random_place_ships([Ship(Ship.Submarine)])
    grid.display(True)

def test_grid_ships():
    grid = Grid()
    grid.random_place_ships([Ship(['XXX']), Ship(['X','X']), Ship()])
    grid.shot(Point(0, 0))
    grid.shot(grid.ships[0].pos)
    grid.display(True)

def test_grid_sunk_ship():
    grid = Grid()
    grid.random_place_ships([Ship(['XXX'])])
    grid.shot(Point(0, 0))
    grid.display(True)
    for p in grid.ships[0].all_positions():
        grid.shot(p)
    grid.display(False)

def main():
    # test_grid_one_ship()
    # test_grid_ships()
    test_grid_sunk_ship()

if __name__ == "__main__":
    main()
