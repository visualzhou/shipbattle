from grid import *

class Game:
    def __init__(self):
        self.grid = Grid()

    def setup(self):
        ships = list(Ship(pattern) for pattern in Ship.ALL_SHIPS_PATTERNS)
        self.grid.random_place_ships(ships)

    def get_input(self):
        pos = input("Guess the location: ")
        try:
            x = int(pos[0])
            y = int(pos[1])
            return Point(x, y)
        except (IndexError, ValueError) as ex:
            print(ex)

        return None

    def is_finished(self):
        return all(s.is_sunk() for s in self.grid.ships)

    def display_live_ships(self):
        for s in self.grid.ships:
            if not s.is_sunk():
                print("-" * 10)
                print("\n".join(s.pattern))

    def play_round(self):
        # while not self.is_finished:
        self.grid.display(False)
        # Print live ships
        self.display_live_ships()

        shot_pos = None
        while shot_pos is None:
            shot_pos = self.get_input()
        shot_result = self.grid.shot(shot_pos)
        print("Shotting at " + str(shot_pos) + " " + str(shot_result))
        print("-"*60)

    def play(self):
        while not self.is_finished():
            self.play_round()

        # while not self.is_finished:
        self.grid.display(True)
        # Print live ships
        self.display_live_ships()


def test_game():
    game = Game()
    game.setup()
    game.play()

def main():
    test_game()


if __name__ == "__main__":
    main()
